package pl.codementors.enty;

import java.util.Objects;

public class Sapling {

    private String species;
    private Type type;

    public Sapling(Ent ent) {
        species = ent.getSpecies();
        type = ent.getTreetype();
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Sapling{" +
                "species='" + species + '\'' +
                ", type=" + type +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sapling sapling = (Sapling) o;
        return Objects.equals(species, sapling.species) &&
                type == sapling.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, type);
    }
}
