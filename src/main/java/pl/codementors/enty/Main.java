package pl.codementors.enty;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        System.out.println("Welcome to our Grove");
        System.out.println("----------");
        Grove grove = new Grove();
        grove.read("Entlist");
        grove.print();
        System.out.println("----------");
        grove.printNames();
        System.out.println("----------");
        grove.printDescription();
        System.out.println("----------");
        System.out.println(grove.count());
        System.out.println("----------");
        System.out.println(grove.count("Three"));
        System.out.println("----------");
        System.out.println(grove.count("Two", Type.CONIFER));
        System.out.println("----------");
        grove.fetch("Three", "Four").forEach(System.out::println);
        System.out.println("----------");
        grove.fetch(Type.LEAFY, "Five").forEach(System.out::println);
        System.out.println("----------");
        grove.sorted().forEach(System.out::println);
        System.out.println("----------");
        grove.sortByAge().forEach(System.out::println);
        System.out.println("----------");
        grove.sortByAgeAndHeight().forEach(System.out::println);
        System.out.println("----------");
        grove.fetchSpecies().forEach(System.out::println);
        System.out.println("----------");
        grove.fetchSpeciesDistinct().forEach(System.out::println);
        System.out.println("----------");
        System.out.println(grove.maxHeight());
        System.out.println(grove.minHeight());
        System.out.println(grove.averageHeight());
        System.out.println(grove.sumHeight());
        System.out.println("----------");
        grove.namelist().forEach(System.out::println);
        System.out.println("----------");
        System.out.println(grove.maxName());
        grove.namesLength().forEach(System.out::println);
        grove.namesFirstChars().forEach(System.out::println);
        System.out.println("----------");
        grove.saplings().forEach(System.out::println);
    }

}