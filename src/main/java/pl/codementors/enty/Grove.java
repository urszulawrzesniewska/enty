package pl.codementors.enty;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.io.FileReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class Grove {

    private static final Logger log = Logger.getLogger(Grove.class.getName());

    private List<Ent> ents = new ArrayList<>();

    public void add(Ent ent) {

        ents.add(ent);
    }

    public void read(String fileName) {

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            while (br.ready()) {
                String treetype = br.readLine();
                String species = br.readLine();
                String heightText = br.readLine();
                String ageText = br.readLine();
                String name = br.readLine();

                Type type = Type.valueOf(treetype.toUpperCase());
                int age = Integer.parseInt(ageText);
                int height = Integer.parseInt(heightText);

                Ent ent = new Ent(type, species, height, age, name);
                ents.add(ent);
            }
        } catch (IOException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
    }

    public void print() {

        ents.forEach(System.out::println);
        System.out.println();
    }

    public void printNames() {
        ents.stream().map(Ent::getName).forEach(System.out::println);
    }

    public void printDescription() {
        ents.forEach(e -> System.out.println(e.getName() + " " + e.getSpecies() + " " + e.getTreetype()));
    }

    public int count() {
        return ents.size();
    }

    public long count(String species) {
        return ents.stream().filter(e -> e.getSpecies().equals(species)).count();
    }

    public long count(String species, Type treetype) {
        return ents.stream()
                .filter(e -> e.getTreetype().equals(treetype))
                .filter(e -> e.getSpecies().equals(species))
                .count();
    }

    public List<Ent> fetch(String... species) {
        List<String> speciesList = Arrays.asList(species);
        return ents.stream().filter(e -> speciesList.contains(e.getSpecies())).collect(Collectors.toList());
    }

    public List<Ent> fetch(Type type, String... species) {
        List<String> speciesList = Arrays.asList(species);
        return ents.stream()
                .filter(e -> e.getTreetype().equals(type))
                .filter(e -> speciesList.contains(e.getSpecies()))
                .collect(Collectors.toList());
    }
    public List<Ent> sorted() {
        return ents.stream().sorted().collect(Collectors.toList());
    }
    public List<Ent> sortByAge() {
        return ents.stream()
                .sorted(Comparator.comparingInt(Ent::getAge)).collect(Collectors.toList());
    }
    public List<Ent> sortByAgeAndHeight() {
        return ents.stream()
                .sorted(Comparator.comparingInt(Ent::getHeight).thenComparing(Ent::getAge))
                .collect(Collectors.toList());
    }
    public Set<String> fetchSpecies() {
        return ents.stream().map(Ent::getSpecies).collect(Collectors.toSet());
    }
    public List<String> fetchSpeciesDistinct() {
        return ents.stream().map(Ent::getSpecies).distinct().sorted().collect(Collectors.toList());
    }
    public int maxHeight() {
        return ents.stream().mapToInt(Ent::getHeight).max().orElse(0);

    }
    public int minHeight() {
        return ents.stream().mapToInt(Ent::getHeight).min().orElse(0);

    }
    public double averageHeight() {
        return ents.stream().mapToInt(Ent::getHeight).average().orElse(0);

    }
    public int sumHeight() {
        return ents.stream().mapToInt(Ent::getHeight).sum();
    }
    public List<String> namelist() {
        return ents.stream().map(Ent::getName).collect(Collectors.toList());
    }
    public String maxName() {
        return ents.stream().map(Ent::getName).max(Comparator.comparingInt(String::length)).orElse(null);
    }
    public List<Integer> namesLength () {
        return ents.stream().map(e -> e.getName().length()).collect(Collectors.toList());
    }
    public List<Character> namesFirstChars() {
        return ents.stream().map(Ent::getName).map(n -> n.charAt(0)).collect(Collectors.toList());
    }
    public List<Sapling> saplings() {
        return ents.stream().map(Sapling::new).collect(Collectors.toList());

    }
}

