package pl.codementors.enty;

import java.util.Objects;

public class Ent implements Comparable<Ent> {

    private Type treetype;
    private String species;
    private int height;
    private int age;
    private String name;

    public Ent(Type treetype, String species, int height, int age, String name) {
        this.treetype = treetype;
        this.species = species;
        this.height = height;
        this.age = age;
        this.name = name;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public Type getTreetype() {
        return treetype;
    }

    public void setTreetype(Type treetype) {
        this.treetype = treetype;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ENT: " +
                "Treetype=" + treetype +
                ", Species='" + species + '\'' +
                ", Height=" + height + "m" +
                ", Age=" + age + "years old" +
                ", Name='" + name + '\'' +
                ' ';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ent ent = (Ent) o;
        return height == ent.height &&
                age == ent.age &&
                treetype == ent.treetype &&
                Objects.equals(species, ent.species) &&
                Objects.equals(name, ent.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(treetype, species, height, age, name);
    }

    @Override
    public int compareTo(Ent o) {
        int ret = this.treetype.compareTo(o.treetype);
        if (ret == 0) {
            ret = this.species.compareTo(o.species);
        }
        if (ret == 0) {
            ret = this.height - o.height;
        }
        if (ret == 0) {
            ret = this.age - o.age;
        }
        if (ret == 0) {
            ret = this.name.compareTo(o.name);
        }
        return ret;

    }
}
